#pragma once

#include<memory>

class CloudsNoise {
public:
	CloudsNoise();
	
	float noise2D(float sample_x, float sample_y);

private:
	std::unique_ptr<int[]> pTable; // таблица перестановок
	//градиенты
	std::unique_ptr<float[]> Gx;
	std::unique_ptr<float[]> Gy;
	std::unique_ptr<float[]> Gz;

	// параметры генерации шума
	struct {
		int octaves;
		float persistence;
		float lacunarity;
		float baseFrequency;
		float baseAmplitude;
		float sample_z;
	} m_noiseParam;

	float noise(float sample_x, float sample_y, float sample_z);
};

