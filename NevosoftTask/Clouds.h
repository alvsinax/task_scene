#pragma once

#include "SDL_PTR.h"
#include "CloudsNoise.h"

#include <vector>
#include <deque>

class Canvas;
class EndlessContiner;

class Clouds {
public:
	Clouds(std::shared_ptr<Canvas> canvas, int fixedAreaY);
	
	void make();
	void tick();	

	void draw(SDL_Renderer * renderer);

private:		
	std::shared_ptr<Canvas> m_canvas;
	std::unique_ptr<CloudsNoise> m_noise;
	std::unique_ptr<EndlessContiner> m_noiseData;		
	float invWidth;
	float invHeight;
	SDL_TexturePtr m_texture;
	int xOffset;
	int fixedAreaY;
	
	void buildNoise();
	void makePixels();	
};

// ��������� ��� �������� ������� ����. 
// ������� ��� �����, ������� - � ����������.
class EndlessContiner {
public:	
	EndlessContiner(int width, int height);
	int size() const { return m_size; }
	// �������� ���� �����
	void insertAndShift(std::vector<float> fields);

	void  set(int x, int y, float value);
	float get(int x, int y) const { return m_noiseData[x][y]; }
	
	float getMin() const { return m_min; }
	float getMax() const { return m_max; }	

	int convertX(int index) const { return index % m_width; }
	int convertY(int index) const { return index / m_width; }

private:
	int m_width, m_height;
	int m_size;
	float m_min, m_max;
	std::deque<std::vector<float>> m_noiseData;
};