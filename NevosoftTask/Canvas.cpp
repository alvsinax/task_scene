#include "Canvas.h"

Canvas::Canvas(int width, int height, Uint32 pixel_format, SDL_TexturePtr texture) :
	m_width(width), m_height(height), m_pitch(width * sizeof(uint32_t)),
	m_pixels(width * height, 0),
	m_texture(std::move(texture))
{ 
	m_pixelFormat = SDL_AllocFormat(pixel_format);
}

Canvas::~Canvas() {
	SDL_FreeFormat(m_pixelFormat);	
}

void Canvas::setPixel(int index, SDL_Color color) {	
	m_pixels[index] = SDL_MapRGBA(m_pixelFormat, color.r, color.g, color.b, color.a);
}

void Canvas::setPixel(SDL_Point pos, SDL_Color color) {	
	//�������� �� ��������
	if (pos.x >= 0 && pos.x <= m_width && pos.y >= 0 && pos.y < m_height) {
		const int offset = pos.y * m_width + pos.x;
		setPixel(offset, color);
	}
}

void Canvas::apply() {	
	SDL_UpdateTexture(m_texture.get(), NULL, &m_pixels[0], m_pitch);
}

//� ���
void Canvas::clear() {
	m_pixels = std::vector<uint32_t>(m_width * m_height, 0);
}

void Canvas::draw(SDL_Renderer *renderer) const {
	SDL_RenderCopy(renderer, m_texture.get(), NULL, NULL);
}