#include "Canvas.h"
#include "Clouds.h"
#include "ParticleSystem.h"

#include <SDL.h>
#include <chrono>
#include <ctime> 
#include <iostream>

using namespace std::chrono_literals;

constexpr int width  = 800;
constexpr int height = 600;

SDL_TexturePtr makeBackground(const char *fPath, SDL_Renderer * renderer);
std::shared_ptr<Canvas> createCanvas(int width, int height, SDL_Renderer * renderer);

int main(int argc, char** argv) {

	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_WindowPtr window = createWindow("scene", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
	SDL_RendererPtr render = createRender(window.get(), -1, SDL_RENDERER_ACCELERATED);
	SDL_TexturePtr background = makeBackground("../img/background.bmp", render.get());	

	// ������������ ��������� �� Y.
	const int fixedAreaY = height - height * 0.22;

	Clouds clouds(createCanvas(width, height, render.get()), fixedAreaY);
	clouds.make();	

	ParticleSystem pSys(createCanvas(width, height, render.get()), fixedAreaY);

	SDL_Event event;
	bool running = true;

	constexpr std::chrono::nanoseconds timestep1(256ms); // ����������� �������� �������
	using clock = std::chrono::high_resolution_clock;
	std::chrono::nanoseconds lag1(0ns);
	constexpr std::chrono::nanoseconds timestep2(32ms); // ����������� �������� ������
	std::chrono::nanoseconds lag2(0ns);
	auto time_start = clock::now();	

	//MAIN LOOP
	while (running) {		

		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT)
				running = false;
			else if (event.type == SDL_MOUSEBUTTONDOWN && SDL_BUTTON(SDL_BUTTON_LEFT)) {
				SDL_Point pos;
				SDL_GetMouseState(&pos.x, &pos.y);
				if (pos.y < fixedAreaY) {
					pSys.spawn(pos);
				}
			}
		}

		auto delta_time = clock::now() - time_start;		
		time_start = clock::now();
		lag1 += std::chrono::duration_cast<std::chrono::nanoseconds>(delta_time);		
		lag2 += std::chrono::duration_cast<std::chrono::nanoseconds>(delta_time);

		if (lag1 >= timestep1) {							
			clouds.tick();
			lag1 -= lag1;
		}				

		if (lag2 >= timestep2) {
			pSys.tick();
			lag2 -= lag2;
		}	

		SDL_SetRenderDrawColor(render.get(), 0, 0, 0, 255);
		SDL_RenderClear(render.get());

		SDL_RenderCopy(render.get(), background.get(), NULL, NULL);
		clouds.draw(render.get());
		pSys.draw(render.get());

		SDL_RenderPresent(render.get());			
	}	
	SDL_Quit();
	return 0;
}

SDL_TexturePtr makeBackground(const char *fPath, SDL_Renderer * renderer) {
	SDL_SurfacePtr tmp = createSurface(fPath);
	return createTexture(renderer, tmp.get());
}

std::shared_ptr<Canvas> createCanvas(int width, int height, SDL_Renderer * renderer) {
	const Uint32 pixel_format = SDL_PIXELFORMAT_ARGB8888;
	SDL_TexturePtr texture = createTexture(renderer, pixel_format, SDL_TEXTUREACCESS_STREAMING, width, height);
	SDL_SetTextureBlendMode(texture.get(), SDL_BLENDMODE_BLEND);
	return std::make_shared<Canvas>(width, height, pixel_format, std::move(texture));
}