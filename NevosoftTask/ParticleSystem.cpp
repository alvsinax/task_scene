#include "ParticleSystem.h"
#include "Canvas.h"

#include "ParticleSystem.h"
#include "Canvas.h"

#include <iostream>

#define _USE_MATH_DEFINES
#include <cmath>

Random rnd;

SDL_Color getColor(Random *rnd) {
	const int min = 0;
	const int max = 255;
	return {
		static_cast<Uint8>(rnd->get(min, max)),
		static_cast<Uint8>(rnd->get(min, max)),
		static_cast<Uint8>(rnd->get(min, max)),
		0xFF,
	};
}

// ������ ������������ � ������� �����
Uint8 calcChanel(Uint8 chanel, int life) {
	if (chanel == 0) chanel = 1;
	double result = (life * 255) / chanel;
	if (result < 0) result = 0; if (result > 255) result = 255;
	return static_cast<Uint8>(result);
}

SDL_Color calcColor(SDL_Color color, int life, int highColor) {
	SDL_Color result = color;
	result.a = calcChanel(result.a, life);
	if (highColor == 1) result.b = calcChanel(result.b, life);
	if (highColor == 2) result.g = calcChanel(result.g, life);
	if (highColor == 3) result.r = calcChanel(result.r, life);
	return result;
}

DefaultEffect::DefaultEffect(SDL_Point pos, std::shared_ptr<Canvas> canvas, int startPosY) :
	m_pos(pos), m_canvas(canvas) {
	// ��������� ������� 
	m_params.power = 10000; // ����� ���-�� ������
	m_params.force = { -0.5f , 2.5f };
	m_params.minLife = 10; 
	m_params.maxLife = 80;
	m_params.minSpeed = 0.0f;
	m_params.maxSpeed = 4.0f;
	m_params.highChanel = rnd.get(1, 3); // ����������� ����
	m_params.lossPower = 50;    // ������ ������ � ������
	m_params.bangPosY = pos.y;  // ��� ��������
	m_params.movedSpeed = 5.0f; // �������� ��� ������.

	m_pos.y = startPosY;	
}

void DefaultEffect::update() {
	for (particle &item : m_data) {
		if (item.lifeTick > 0) {			
			item.color = calcColor(item.color, item.lifeTick, m_params.highChanel);
			m_canvas->setPixel(item.pos.toSDLPoint(), item.color);
		}
	}
}

void DefaultEffect::tick() {

	int count = m_params.lossPower;
	if (m_pos.y <= m_params.bangPosY) count = m_params.power - m_params.power / m_params.lossPower;

	if (m_params.power >= count) {
		emit(count);
		m_params.power -= count;
	}
	else {
		emit(count);
		m_params.power = 0;
	}

	lifeCount = 0;

	m_pos.y-=5;

	for (particle &item : m_data) {
		if (item.lifeTick > 0) {

			item.pos.x += item.direction.x * item.speed + m_params.force.x;
			item.pos.y += item.direction.y * item.speed + m_params.force.y;

			m_canvas->setPixel(item.pos.toSDLPoint(), item.color);
			item.lifeTick--;
			lifeCount++;
		}
	}
}

bool DefaultEffect::isLife() const {
	return m_params.power > 0 || lifeCount > 0;
}

void DefaultEffect::emit(int power) {
	for (int i = 0; i < power; ++i) {
		auto angle = rnd.get(0.0f, 360.0f) * M_PI * 2;
		float x = cos(angle);
		float y = sin(angle);
		m_data.push_back({ rnd.get(m_params.minLife, m_params.maxLife), m_pos, getColor(&rnd), { x , y }, rnd.get(m_params.minSpeed, m_params.maxSpeed) });
	}
}

//-------------------------------------------------------------------
ParticleSystem::ParticleSystem(std::shared_ptr<Canvas> canvas, int fixedAreaY) :
	m_canvas(canvas), fixedAreaY(fixedAreaY)
{}

void ParticleSystem::tick() {
	m_canvas->clear();
	auto it = std::begin(m_data);
	while (it != std::end(m_data)) {
		if (it->operator->()->isLife()) {
			it->operator->()->tick();
			it++;
		}
		else {
			it = m_data.erase(it);
		}
	}
	m_canvas->apply();
}

void ParticleSystem::draw(SDL_Renderer * renderer) {
	for (auto &item : m_data) {
		if (item->isLife()) {
			item->update();
		}
	}
	m_canvas->apply();
	m_canvas->draw(renderer);
}

void ParticleSystem::spawn(SDL_Point pos) {
	m_data.push_back(std::make_unique<DefaultEffect>(pos, m_canvas, fixedAreaY));
}