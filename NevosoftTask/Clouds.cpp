#include "Clouds.h"
#include "Canvas.h"

#include <algorithm>
#include <iostream>

Clouds::Clouds(std::shared_ptr<Canvas> canvas, int fixedAreaY) :
	m_canvas(canvas),	  
	m_noise(new CloudsNoise()), m_noiseData(new EndlessContiner(canvas->width(), canvas->heigh())),	  	
	invWidth(1.0f / float(canvas->width())), invHeight(1.0f / float(canvas->heigh())), 
	xOffset(0), fixedAreaY(fixedAreaY)
{ }

void Clouds::make() {
	buildNoise();
	makePixels();
	m_canvas->apply();
}

// �������� ��� �� 1 ������� �����
void Clouds::tick() {
	xOffset++;	
	int x = xOffset + m_canvas->width();
	std::vector<float> newTail(m_canvas->heigh());
	for (int y = 0; y < newTail.size(); ++y) {
		float noise = m_noise->noise2D(float(x)*invWidth, float(y)*invHeight);
		newTail[y] = noise;
	}	

	m_noiseData->insertAndShift(newTail);
	makePixels();
	m_canvas->apply();
}

void Clouds::draw(SDL_Renderer * renderer) {
	m_canvas->draw(renderer);
}

void Clouds::buildNoise() {
	for (int i = 0; i < m_noiseData->size(); ++i) {
		int x = m_noiseData->convertX(i);
		int y = m_noiseData->convertY(i);
		float noise = m_noise->noise2D(float(x)*invWidth, float(y)*invHeight);
		m_noiseData->set(x, y, noise);		
	}	
}

// TODO: � �� ���� ��� � SDL �������� ��������!
void Clouds::makePixels() {
	// Convert noise values to pixel colour values.
	float temp = 1.0f / (m_noiseData->getMax() - m_noiseData->getMin());
	// ������� �������� ��������
	for (int i = 0; i < m_noiseData->size(); ++i) {
		int x = m_noiseData->convertX(i);
		int y = m_noiseData->convertY(i);

		// "Stretch" the gaussian distribution of noise values to better fill -1 to 1 range.
		float noise = m_noiseData->get(x, y);
		noise = -1.0f + 2.0f*(noise - m_noiseData->getMin())*temp;
		// Remap to RGB friendly colour values in range between 0 and 1.
		noise += 1.0f;
		noise *= 0.5f;		

		const Uint8 colourByte = Uint8(noise * 170); // ����������� ������
		SDL_Color color = { colourByte, colourByte, colourByte, colourByte };
		if (y > fixedAreaY) {
			color = { 0xFF, 0, 0, 0 };					
		}
		m_canvas->setPixel({ x, y }, color);
	}	
}

//--------------------------------------------------------------
EndlessContiner::EndlessContiner(int width, int height) : 
	m_width(width), m_height(height), 
	m_size(width * height), 
	m_min(0.0f), m_max(0.0f)
{	
	for (int i = 0; i < width; i++) {
		m_noiseData.push_back(std::vector<float>(height));
	}		
}

void EndlessContiner::insertAndShift(std::vector<float> fields) {
	m_noiseData.push_back(fields);
	m_noiseData.pop_front();
	
	float addMin = *std::min_element(std::begin(fields), std::end(fields));
	float addMax = *std::max_element(std::begin(fields), std::end(fields));

	if (addMin < m_min) m_min = addMin;
	if (addMax > m_max) m_max = addMax;
}

void EndlessContiner::set(int x, int y, float value) {
	m_noiseData[x][y] = value;	
	if (value < m_min) m_min = value;
	if (value > m_max) m_max = value;
}


