#pragma once

#include "SDL_PTR.h"
#include <vector>

class Canvas {
public:
	Canvas(int width, int height, Uint32 pixel_format, SDL_TexturePtr texture);
	~Canvas();

	int width() const { return m_width;  }
	int heigh() const { return m_height; }
	int size()  const { return m_width * m_height; }

	void setPixel(int index, SDL_Color color);
	void setPixel(SDL_Point pos, SDL_Color color);

	void apply();
	void clear();

	void draw(SDL_Renderer *renderer) const;

private:
	int m_width, m_height, m_pitch;
	std::vector<uint32_t> m_pixels;	
	SDL_TexturePtr     m_texture;
	SDL_PixelFormat* m_pixelFormat;
};