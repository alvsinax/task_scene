#pragma once

#include <SDL.h>
#include <memory>

struct sdl_deleter {
	void operator()(SDL_Window *p)      const { SDL_DestroyWindow(p);   }
	void operator()(SDL_Renderer *p)    const { SDL_DestroyRenderer(p); }
	void operator()(SDL_Texture *p)     const { SDL_DestroyTexture(p);  }
	void operator()(SDL_Surface *p)     const { SDL_FreeSurface(p);     }
	void operator()(SDL_PixelFormat *p) const { SDL_FreeFormat(p);      }
};

using SDL_WindowPtr      = std::unique_ptr<SDL_Window, sdl_deleter>;
using SDL_RendererPtr    = std::shared_ptr<SDL_Renderer>;
using SDL_TexturePtr     = std::unique_ptr<SDL_Texture, sdl_deleter>;
using SDL_SurfacePtr     = std::unique_ptr<SDL_Surface, sdl_deleter>;
using SDL_PixelFormatPtr = std::shared_ptr<SDL_PixelFormat>;

inline SDL_WindowPtr createWindow(const char *title, int x, int y, int w, int h, Uint32 flags) {
	return std::unique_ptr<SDL_Window, sdl_deleter>(
		SDL_CreateWindow(title, x, y, w, h, flags),
		sdl_deleter());
}

inline SDL_RendererPtr createRender(SDL_Window * window, int index, Uint32 flags) {
	return std::shared_ptr<SDL_Renderer>(
		SDL_CreateRenderer(window, index, flags), 
		sdl_deleter());
}

inline SDL_TexturePtr createTexture(SDL_Renderer * renderer,	Uint32 format, int access, int w,	int h) {
	return std::unique_ptr<SDL_Texture, sdl_deleter>(
		SDL_CreateTexture(renderer, format, access, w, h),
		sdl_deleter());
}

inline SDL_TexturePtr createTexture(SDL_Renderer * renderer, SDL_Surface * surface) {
	return std::unique_ptr<SDL_Texture, sdl_deleter>(
		SDL_CreateTextureFromSurface(renderer, surface),
		sdl_deleter());
}

inline SDL_SurfacePtr createSurface(const char* fName) {
	return std::unique_ptr<SDL_Surface, sdl_deleter>(
		SDL_LoadBMP(fName),
		sdl_deleter());
}

inline SDL_PixelFormatPtr createPixelFormat(Uint32 pixel_format) {
	return std::shared_ptr<SDL_PixelFormat>(
		SDL_AllocFormat(pixel_format),
		sdl_deleter());
}