#pragma once

#include "SDL_PTR.h"

#include <list>
#include <vector>
#include <random>
#include <ctime>

struct Point2F {
	float x;
	float y;

	SDL_Point toSDLPoint() const {
		return { static_cast<int>(x), static_cast<int>(y) };
	}

	Point2F(SDL_Point p) : x(p.x), y(p.y) {};
	Point2F(float x = 0.0f, float y = 0.0f) : x(x), y(y) {}
};

struct particle {
	int lifeTick;
	Point2F pos;
	SDL_Color color;
	Point2F direction;
	float speed;
};

class Canvas;

class Random {
public:
	Random() : gen(time(0)) {}
	int get(int min, int max) {
		std::uniform_int_distribution<> gi(min, max);
		return gi(gen);
	}
	float get(float min, float max) {
		std::uniform_real_distribution<> gr(min, max);
		return gr(gen);
	}
private:
	std::mt19937 gen;
};

class IEffect {
public:
	virtual ~IEffect() = default;
	virtual void update() = 0;
	virtual void tick() = 0;
	virtual bool isLife() const = 0;
};

class DefaultEffect : public IEffect {
public:
	DefaultEffect(SDL_Point pos, std::shared_ptr<Canvas> canvas, int startPosY);
	~DefaultEffect() = default;
	void update() override;
	void tick() override;
	bool isLife() const override;
private:
	Point2F m_pos;
	std::shared_ptr<Canvas> m_canvas;
	std::vector<particle> m_data;
	int lifeCount;	
	struct {
		int power;
		Point2F force;
		int minLife;
		int maxLife;
		float minSpeed;
		float maxSpeed;
		int highChanel;
		int lossPower; 
		int bangPosY;
		float movedSpeed;
	} m_params;

	void emit(int power);
};

class ParticleSystem {
public:
	ParticleSystem(std::shared_ptr<Canvas> canvas, int fixedAreaY);
	void tick();
	void draw(SDL_Renderer * renderer);
	void spawn(SDL_Point pos);
private:
	std::shared_ptr<Canvas> m_canvas;
	std::list<std::unique_ptr<IEffect>> m_data;
	int fixedAreaY;
};

